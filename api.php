<?php
define('DOMAIN_TEST', 'restfs-jaspercastillo.c9.io');
define('DOMAIN_PROD', 'mvn.quillbytes.com');

define('MEDIA_TYPE_JSON', 'application/json');
define('DEFAULT_PATH', '/');
define('API_PATH', 'api');
define('DOWNLOAD_PATH', 'repository');

$domain = getDomain();
if ($domain == DOMAIN_TEST) {
    define('WORKING_DIR', '/tmp/restfs');
} else {
    define('WORKING_DIR', '/home/yuuzaico/public_html/mvn.quillbytes.com/repository');
}


$EXCLUDED_ENTRIES = array('.', '..');

$statusCode = 200;
$fileResource = false;
$errors = array();
header('Content-Type: ' . MEDIA_TYPE_JSON);

if (!file_exists(WORKING_DIR)) {
    mkdir(WORKING_DIR, 0777, true);
}

function addError($message, $params) {
    global $errors;
    $errors[] = array('message' => $message, 'params' => $params);
}

function getRequestMethod() {
    return $_SERVER['REQUEST_METHOD'];
}

function validateRequestMethod($method) {
    if ($method != 'GET') {
        throwMethodNotAllowed($method);
        return false;
    }
    return true;
}

function throwMethodNotAllowed($method) {
    global $statusCode;
    $statusCode = 405;
    addError("Method not allowed", array($method));
    header('Allowed: GET');
}

function throwNotFound($path) {
    global $statusCode;
    $statusCode = 404;
    addError("Resource not found", array($path));
}

function getPathFromParameter($addSlash = true) {
    $resources = array_filter(explode('/', $_SERVER['REQUEST_URI']));
    array_shift($resources);
    $path = implode('/', $resources);
    if ($addSlash) {
        $path = "/$path";
    }
    return $path;
}

function getFullPath() {
    return sprintf('%s%s', WORKING_DIR, getPathFromParameter());
}

function listFiles($dir) {
    global $fileResource;
    if (!file_exists($dir)) {
        throwNotFound(getPathFromParameter());
        return array();
    }
    if (!is_dir($dir)) {
        $fileResource = true;
        return array(basename($dir));
    }
    return array_diff(scandir($dir), getExcluded());
}

function getExcluded() {
    global $EXCLUDED_ENTRIES;
    return $EXCLUDED_ENTRIES;
}

function isDirectory($entry) {
    $base = getFullPath();
    return is_dir(sprintf('%s/%s', $base, $entry));
}

function buildMetadata($data) {
    global $statusCode;
    $path = getPathFromParameter();
    return array('status' => $statusCode, 'path' => $path, 'count' => count($data));
}

function buildResponse($files = array()) {
    global $errors;
    $data = buildFiles($files);
    $metadata = buildMetadata($data);
    // $response = array('metadata' => $metadata, 'data' => $data, 'errors' = $errors);
    $response = array('metadata' => $metadata, 'data' => $data, 'errors' => $errors);
    return $response;
}

function buildFiles($files) {
    $data = array();
    foreach ($files as $entry) {
        $data[] = buildEntry($entry);
    }
    return $data;
}

function buildEntry($entry) {
    $isDirectory = isDirectory($entry);
    $obj = array('isDirectory' => $isDirectory, 'filename' => $entry);
    if (!$isDirectory) {
        $obj['_links'] = array('rel' => 'download', 'href' => getDownloadUrl($entry));
    } else {
        $obj['_links'] = array('rel' => 'subdirectory', 'href' => getChildUrl($entry));
    }
    return $obj;
}

function getProtocol() {
    return isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http';
}

function getDomain() {
    return $_SERVER['SERVER_NAME'];
}

function getChildUrl($child) {
    $protocol = getProtocol();
    $domain = getDomain();
    $path = getPathFromParameter(false);
    if (strlen($path) > 0) {
        $path = "$path/";
    }
    return sprintf("%s://%s/%s/%s%s", $protocol, $domain, API_PATH, $path, $child);
}

function getDownloadUrl($file) {
    $protocol = getProtocol();
    $domain = getDomain();
    $fileUrl = getFileUrl($file);
    return sprintf("%s://%s/%s", $protocol, $domain, $fileUrl);
}

function getFileUrl($file) {
    global $fileResource;
    $path = getPathFromParameter(false);
    if (!$fileResource && strlen($path) > 0) {
        $path = "$path/";
    }
    return $fileResource ? sprintf("%s/%s", DOWNLOAD_PATH, $path) : sprintf("%s/%s%s", DOWNLOAD_PATH, $path, $file);
}

function encodeJson($obj) {
    return json_encode($obj, JSON_UNESCAPED_SLASHES);
}

$files = array();
$method = getRequestMethod();
if (validateRequestMethod($method)) {
    $path = getFullPath();
    $files = listFiles($path);
}

http_response_code($statusCode);
$response = buildResponse($files);
echo encodeJson($response);
?>